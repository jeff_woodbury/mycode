terraform {
  cloud {
    organization = "jeffWoodbury"

    workspaces {
      name = "my-example"
    }
  }
}
